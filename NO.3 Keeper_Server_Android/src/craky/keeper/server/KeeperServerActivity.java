package craky.keeper.server;

import java.io.File;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import craky.keeper.util.Util;
import craky.keeper.util.Util.ServiceActionResult;

public class KeeperServerActivity extends Activity
{
    private TextView txtStatus;

    private Button btnServer;

    private Resources resources;

    private boolean serviceRunning;

    private IntentFilter intentFilter;

    private BroadcastReceiver intentReceiver = new BroadcastReceiver()
    {
        public void onReceive(Context context, Intent intent)
        {
            String result = intent.getStringExtra(Util.SERVICE_ACTION_RESULT_KEY);
            dealActionResult(result);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keeper_server);
        this.resources = getResources();
        this.txtStatus = (TextView)findViewById(R.id.txtStatus);
        this.btnServer = (Button)findViewById(R.id.btnServer);
        this.serviceRunning = Util.isKeeperServiceRunning(this);
        switchText();

        if(Util.hasExternalStorage())
        {
            File externalStorage = Environment.getExternalStorageDirectory();
            Util.keeperDataParentPath = externalStorage.getAbsolutePath() + "/Keeper";
            Util.keeperDataPath = Util.keeperDataParentPath + "/data";
            btnServer.setEnabled(true);
        }
        else
        {
            btnServer.setEnabled(false);
            String info = resources.getString(R.string.external_storage_not_exists);
            txtStatus.setText(info);

            if(serviceRunning)
            {
                startOrStopService(false);
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        intentFilter = new IntentFilter(Util.MESSAGE_KEY);
        registerReceiver(intentReceiver, intentFilter);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        unregisterReceiver(intentReceiver);
    }

    private void dealActionResult(String result)
    {
        if(ServiceActionResult.Start_OK.toString().equals(result))
        {
            this.serviceRunning = true;
        }
        else if(ServiceActionResult.Stop_OK.toString().equals(result))
        {
            this.serviceRunning = false;
        }
        else if(ServiceActionResult.Start_Exception.toString().equals(result))
        {
            Util.showToast(this, resources.getString(R.string.start_server_error));
            this.serviceRunning = Util.isKeeperServiceRunning(this);
        }
        else if(ServiceActionResult.Stop_Exception.toString().equals(result))
        {
            Util.showToast(this, resources.getString(R.string.stop_server_error));
            this.serviceRunning = Util.isKeeperServiceRunning(this);
        }
        else if(ServiceActionResult.Copy_File_Error.toString().equals(result))
        {
            Util.showToast(this, resources.getString(R.string.copy_data_error));
            this.serviceRunning = false;
        }

        switchText();
        btnServer.setEnabled(true);
    }

    public void switchServer(View view)
    {
        startOrStopService(!serviceRunning);
    }

    private void startOrStopService(boolean start)
    {
        btnServer.setEnabled(false);

        if(start)
        {
            txtStatus.setText(resources.getString(R.string.server_starting));
            startService(new Intent(getBaseContext(), KeeperService.class));
        }
        else
        {
            KeeperService.stopNormal = true;
            txtStatus.setText(resources.getString(R.string.server_stopping));
            stopService(new Intent(getBaseContext(), KeeperService.class));
        }
    }

    private void switchText()
    {
        if(serviceRunning)
        {
            String localIP = Util.getLocalhostIP();
            localIP = localIP == null? "": '(' + localIP + ')';
            btnServer.setText(resources.getString(R.string.stop_server));
            String info = resources.getString(R.string.server_started) + localIP + "\n" + resources.getString(R.string.data_path) + Util.keeperDataPath;
            txtStatus.setText(info);
        }
        else
        {
            btnServer.setText(resources.getString(R.string.start_server));
            txtStatus.setText(resources.getString(R.string.server_stopped));
        }
    }
}