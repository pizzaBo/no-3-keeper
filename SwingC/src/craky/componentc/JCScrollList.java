package craky.componentc;

import java.awt.Color;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import craky.util.UIResourceManager;

public class JCScrollList extends JCScrollPane
{
    private static final long serialVersionUID = -1708752774205915517L;

    private static final Color DISABLED_BG = UIResourceManager.getColor(UIResourceManager.KEY_TEXT_DISABLED_BACKGROUND);
    
    private Border border;
    
    private Border disabledBorder;
    
    private Color background;
    
    protected JCList list;
    
    public JCScrollList(JCList list)
    {
        super();
        setViewportView(this.list = list);
        init();
    }
    
    public JCScrollList()
    {
        this(new JCList());
    }
    
    public JCScrollList(Vector<?> listData)
    {
        this(new JCList(listData));
    }
    
    public JCScrollList(ListModel dataModel)
    {
        this(new JCList(dataModel));
    }
    
    private void init()
    {
        setBorder(new LineBorder(new Color(84, 165, 213)));
        setDisabledBorder(new LineBorder(new Color(84, 165, 213, 128)));
        setBackground(UIResourceManager.getWhiteColor());
        setHeaderVisible(false);
        list.setBorder(new EmptyBorder(0, 0, 0, 0));
        list.setDisabledBorder(list.getBorder());
        list.setVisibleInsets(0, 0, 0, 0);
        list.setAlpha(0.0f);
    }
    
    public Border getDisabledBorder()
    {
        return disabledBorder;
    }

    public void setDisabledBorder(Border disabledBorder)
    {
        this.disabledBorder = disabledBorder;
        
        if(!this.isEnabled())
        {
            super.setBorder(disabledBorder);
        }
    }
    
    public void setBorder(Border border)
    {
        this.border = border;
        super.setBorder(border);
    }
    
    public Color getDisabledForeground()
    {
        return list.getDisabledForeground();
    }

    public void setDisabledForeground(Color disabledForeground)
    {
        list.setDisabledForeground(disabledForeground);
    }
    
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        super.setBorder(enabled? border: disabledBorder);
        super.setBackground(enabled? background: DISABLED_BG);
    }
    
    public void setBackground(Color background)
    {
        this.background = background;
        super.setBackground(background);
    }
    
    public JCList getList()
    {
        return list;
    }
}