package craky.componentc;

import java.awt.Color;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import craky.util.UIResourceManager;
import craky.util.UIUtil;

public class JCToggleButton extends JToggleButton
{
    private static final long serialVersionUID = -393947872129394154L;

    private Image image;

    private Image disabledImage;

    private Image disabledSelectedImage;

    private Image pressedImage;

    private Image selectedImage;

    private Image rolloverImage;

    private Image rolloverSelectedImage;
    
    private float alpha;

    private Insets imageInsets;
    
    private Color disabledTextColor;

    private boolean imageOnly;
    
    private boolean paintPressDown;

    public JCToggleButton()
    {
        this(null, null, false);
    }

    public JCToggleButton(Icon icon)
    {
        this(null, icon, false);
    }

    public JCToggleButton(Icon icon, boolean selected)
    {
        this(null, icon, selected);
    }

    public JCToggleButton(String text)
    {
        this(text, null, false);
    }

    public JCToggleButton(String text, boolean selected)
    {
        this(text, null, selected);
    }

    public JCToggleButton(Action a)
    {
        this();
        setAction(a);
    }

    public JCToggleButton(String text, Icon icon)
    {
        this(text, icon, false);
    }

    public JCToggleButton(String text, Icon icon, boolean selected)
    {
        super(text, icon, selected);
        setUI(new CToggleButtonUI());
        alpha = 1.0f;
        imageOnly = true;
        paintPressDown = true;
        imageInsets = new Insets(3, 3, 3, 3);
        disabledTextColor = new Color(103, 117, 127);
        setForeground(new Color(0, 28, 48));
        setBackground(Color.GRAY);
        setBorder(new EmptyBorder(0, 0, 0, 0));
        setContentAreaFilled(false);
        setFont(UIUtil.getDefaultFont());
        setFocusable(false);
        setRolloverEnabled(true);
        setIconTextGap(5);
        setMargin(new Insets(0, 0, 0, 0));
        super.setOpaque(false);
        image = UIResourceManager.getImageByName("button_normal.png", true);
        disabledImage = UIResourceManager.getImageByName("button_disabled.png", true);
        disabledSelectedImage = UIResourceManager.getImageByName("togglebutton_selected.png", true);
        selectedImage = UIResourceManager.getImageByName("togglebutton_selected.png", true);
        rolloverImage = UIResourceManager.getImageByName("button_rollover.png", true);
    }

    public Image getImage()
    {
        return image;
    }

    public void setImage(Image image)
    {
        this.image = image;
        this.repaint();
    }

    public Image getDisabledImage()
    {
        return disabledImage;
    }

    public void setDisabledImage(Image disabledImage)
    {
        this.disabledImage = disabledImage;
        this.repaint();
    }

    public Image getDisabledSelectedImage()
    {
        return disabledSelectedImage;
    }

    public void setDisabledSelectedImage(Image disabledSelectedImage)
    {
        this.disabledSelectedImage = disabledSelectedImage;
        this.repaint();
    }

    public Image getPressedImage()
    {
        return pressedImage;
    }

    public void setPressedImage(Image pressedImage)
    {
        this.pressedImage = pressedImage;
        this.repaint();
    }

    public Image getSelectedImage()
    {
        return selectedImage;
    }

    public void setSelectedImage(Image selectedImage)
    {
        this.selectedImage = selectedImage;
        this.repaint();
    }

    public Image getRolloverImage()
    {
        return rolloverImage;
    }

    public void setRolloverImage(Image rolloverImage)
    {
        this.rolloverImage = rolloverImage;
        this.repaint();
    }

    public Image getRolloverSelectedImage()
    {
        return rolloverSelectedImage;
    }

    public void setRolloverSelectedImage(Image rolloverSelectedImage)
    {
        this.rolloverSelectedImage = rolloverSelectedImage;
        this.repaint();
    }

    public float getAlpha()
    {
        return alpha;
    }

    public void setAlpha(float alpha)
    {
        if(alpha >= 0.0f && alpha <= 1.0f)
        {
            this.alpha = alpha;
            this.repaint();
        }
        else
        {
            throw new IllegalArgumentException("Invalid alpha:" + alpha);
        }
    }

    public Insets getImageInsets()
    {
        return imageInsets;
    }

    public void setImageInsets(int top, int left, int bottom, int right)
    {
        this.imageInsets.set(top, left, bottom, right);
        this.repaint();
    }
    
    public boolean isPaintPressDown()
    {
        return paintPressDown;
    }

    public void setPaintPressDown(boolean paintPressDown)
    {
        this.paintPressDown = paintPressDown;
    }

    public Color getDisabledTextColor()
    {
        return disabledTextColor;
    }

    public void setDisabledTextColor(Color disabledTextColor)
    {
        this.disabledTextColor = disabledTextColor;
        
        if(!this.isEnabled())
        {
            this.repaint();
        }
    }

    public boolean isImageOnly()
    {
        return imageOnly;
    }

    public void setImageOnly(boolean imageOnly)
    {
        this.imageOnly = imageOnly;
        this.repaint();
    }
    
    @Deprecated
    public void updateUI()
    {}
    
    @Deprecated
    public void setOpaque(boolean opaque)
    {}
}