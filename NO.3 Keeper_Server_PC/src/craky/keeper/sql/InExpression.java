package craky.keeper.sql;

public class InExpression extends CollectionExpression
{
    private static final long serialVersionUID = -8772640872291232757L;

    public InExpression(String columnName, Object[] values)
    {
        super(columnName, values);
    }

    protected String getOperation()
    {
        return "in";
    }
}