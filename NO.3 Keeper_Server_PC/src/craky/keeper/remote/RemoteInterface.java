package craky.keeper.remote;

import java.util.List;

import craky.keeper.bean.Category;
import craky.keeper.bean.Income;
import craky.keeper.bean.Pay;
import craky.keeper.bean.ResultSet;
import craky.keeper.bean.User;
import craky.keeper.sql.Criterion;

public interface RemoteInterface
{
    public ResultSet login(String name, String password);

    public boolean isFirstUse();

    public List<User> getUsers(Criterion...criterions);

    public User addUser(User user);

    public void deleteUser(User user);

    public User updateUser(User user);

    public ResultSet getPartiallyPays(long cachedUpdateTime, int resultCount, Criterion...criterions);

    public List<Pay> getPays(Criterion...criterions);

    public ResultSet addPay(Pay pay, Category category);

    public ResultSet deletePay(Pay pay, Category category);

    public ResultSet updatePay(Pay pay, Category oldCategory, Category newCategory);

    public ResultSet getPartiallyIncomes(long cachedUpdateTime, int resultCount, Criterion...criterions);

    public List<Income> getIncomes(Criterion...criterions);

    public ResultSet addIncome(Income income, Category category);

    public ResultSet deleteIncome(Income income, Category category);

    public ResultSet updateIncome(Income income, Category oldCategory, Category newCategory);

    public ResultSet getCategorys(boolean includeIncome);

    public Category addCategory(Category category);

    public Category deleteCategory(Category category, boolean isPay);

    public boolean existCategory(boolean isPay, String name);
}