package craky.keeper.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class GradientPaintRelativeLayout extends RelativeLayout
{
    private final float[] POSITIONS = {0.1f, 0.80f};

    private final int[] COLORS = {0xFFACE7EF, 0xFFE7D9C5};

    private Paint paint;

    private Shader shader;

    public GradientPaintRelativeLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.setWillNotDraw(false);
        this.paint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        int width = canvas.getWidth();
        int height = canvas.getHeight();

        if(shader == null)
        {
            shader = new LinearGradient(0, 0, 0, height, COLORS, POSITIONS, Shader.TileMode.CLAMP);
            paint.setShader(shader);
        }

        canvas.drawRect(0, 0, width, height, paint);
    }
}