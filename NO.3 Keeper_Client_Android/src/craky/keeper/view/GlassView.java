package craky.keeper.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class GlassView extends View
{
    public GlassView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
    }
}