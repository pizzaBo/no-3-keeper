package craky.keeper.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewParent;
import android.widget.RelativeLayout;

public class ListItem extends RelativeLayout
{
    private boolean unselectable;

    private int position;

    public ListItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.unselectable = true;
    }

    public void forcePressed()
    {
        super.setPressed(true);
    }

    @Override
    public void setPressed(boolean pressed)
    {
        ViewParent parent = this.getParent();

        if((!pressed && unselectable) || (pressed && (!(parent instanceof KeeperListView) || ((KeeperListView)parent).isPressable())))
        {
            super.setPressed(pressed);
        }
    }

    public boolean isUnselectable()
    {
        return this.unselectable;
    }

    public void setUnselectable(boolean unselectable)
    {
        this.unselectable = unselectable;
    }

    public int getPosition()
    {
        return this.position;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }
}