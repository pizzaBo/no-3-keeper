package craky.keeper.client.task;

import android.content.res.Resources;
import android.os.AsyncTask;

import com.caucho.hessian.client.HessianProxyFactory;

import craky.keeper.bean.ResultSet;
import craky.keeper.bean.User;
import craky.keeper.client.LoginActivity;
import craky.keeper.client.R;
import craky.keeper.remote.KeeperDBLocalRemote;
import craky.keeper.remote.RemoteInterface;
import craky.keeper.util.KeeperUtil;

public class LoginTask extends AsyncTask<String, String, User>
{
    private LoginActivity activity;

    private long loginTime;

    private String errorMessage;

    public LoginTask(LoginActivity activity, long loginTime)
    {
        this.activity = activity;
        this.loginTime = loginTime;
    }

    private User doLogin(String ip, String username, String psdCleartext)
    {
        KeeperUtil.currentUser = null;
        Resources resources = activity.getResources();

        try
        {
            if(KeeperUtil.remote == null)
            {
                int port = KeeperUtil.getCustomPort();
                HessianProxyFactory factory = new HessianProxyFactory();
                factory.setHessian2Reply(false);
                KeeperUtil.remote = (RemoteInterface)factory.create(RemoteInterface.class, "http://" + ip + ":" + port + "/Keeper");
            }

            String password = User.createCiphertext(username, psdCleartext);
            ResultSet resultSet = KeeperUtil.remote.login(username, password);
            User user = resultSet.getUser();

            if(user == null)
            {
                this.errorMessage = resources.getString(R.string.user_not_exist);
            }
            else if(user.getPassword() == null)
            {
                this.errorMessage = resources.getString(R.string.password_incorrect);
            }
            else
            {
                KeeperUtil.currentUser = user;
                KeeperUtil.currentIp = ip;
                KeeperUtil.cacheCategories(resultSet);
                activity.saveLoginHistory();
            }
        }
        catch(Exception e)
        {
            String dataParent = null;
            boolean tryAgain = false;

            if(ip.equals("127.0.0.1") && (dataParent = KeeperUtil.getLocalDatasParent()) != null)
            {
                try
                {
                    KeeperUtil.remote = new KeeperDBLocalRemote(dataParent);
                    tryAgain = true;
                }
                catch(Throwable t)
                {}
            }

            if(tryAgain)
            {
                doLogin(ip, username, psdCleartext);
            }
            else
            {
                KeeperUtil.remote = null;
                this.errorMessage = resources.getString(R.string.connection_error);
            }
        }

        return KeeperUtil.currentUser;
    }

    @Override
    protected User doInBackground(String...params)
    {
        return doLogin(params[0], params[1], params[2]);
    }

    @Override
    protected void onPostExecute(User user)
    {
        activity.dealLoginResult(this, errorMessage, user);
    }

    public long getLoginTime()
    {
        return this.loginTime;
    }
}