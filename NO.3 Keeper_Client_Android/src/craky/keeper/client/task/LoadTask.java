package craky.keeper.client.task;

import java.util.ArrayList;
import java.util.List;

import android.content.res.Resources;
import android.os.AsyncTask;
import craky.keeper.bean.KeeperObject;
import craky.keeper.bean.ResultSet;
import craky.keeper.bean.User;
import craky.keeper.client.R;
import craky.keeper.sql.Criterion;
import craky.keeper.sql.Restrictions;
import craky.keeper.util.KeeperUtil;
import craky.keeper.view.KeeperListView;

public class LoadTask extends AsyncTask<Boolean, String, ResultSet>
{
    public static final int ITEMS_PER_LOAD = 20;

    private KeeperListView listView;

    private long actionTime;

    private boolean isPay, loadMore;

    private String errorMessage;

    private List<Integer> dataIDs;

    public LoadTask(KeeperListView listView, long actionTime, boolean loadMore, List<Integer> dataIDs)
    {
        this.listView = listView;
        this.actionTime = actionTime;
        this.loadMore = loadMore;
        this.dataIDs = dataIDs;
    }

    @Override
    protected ResultSet doInBackground(Boolean...params)
    {
        Resources resources = listView.getResources();
        ResultSet resultSet = null;

        try
        {
            isPay = params[0];
            long lastUpdateTime = loadMore? listView.getLastUpdateTime(): 0;
            Criterion idNotIn = dataIDs == null? null: Restrictions.ni("id", dataIDs);

            if(isPay)
            {
                if(KeeperUtil.currentUser.getPurview() >= User.VISITOR)
                {
                    Criterion typeNotEquals = Restrictions.ne("type", KeeperUtil.TYPE_EXTRA);
                    Criterion[] criterions = idNotIn == null? new Criterion[]{typeNotEquals}: new Criterion[]{typeNotEquals, idNotIn};
                    resultSet = KeeperUtil.remote.getPartiallyPays(lastUpdateTime, ITEMS_PER_LOAD, criterions);
                }
                else
                {
                    Criterion[] criterions = idNotIn == null? new Criterion[]{}: new Criterion[]{idNotIn};
                    resultSet = KeeperUtil.remote.getPartiallyPays(lastUpdateTime, ITEMS_PER_LOAD, criterions);
                }
            }
            else if(KeeperUtil.currentUser.getPurview() < User.VISITOR)
            {
                Criterion[] criterions = idNotIn == null? new Criterion[]{}: new Criterion[]{idNotIn};
                resultSet = KeeperUtil.remote.getPartiallyIncomes(lastUpdateTime, ITEMS_PER_LOAD, criterions);
            }
        }
        catch(Exception e)
        {
            this.errorMessage = resources.getString(R.string.connection_error);
        }

        return resultSet;
    }

    @Override
    protected void onPostExecute(ResultSet resultSet)
    {
        if(resultSet != null && (!isPay || (isPay && listView.isFirstFinished())))
        {
            KeeperUtil.cacheCategories(resultSet);
        }

        if(loadMore)
        {
            listView.dealLoadMoreResult(this, errorMessage, resultSet);
        }
        else
        {
            List<? extends KeeperObject> dataList;

            if(resultSet == null)
            {
                dataList = new ArrayList<KeeperObject>();
            }
            else
            {
                dataList = isPay? resultSet.getPayList(): resultSet.getIncomeList();
                listView.setLastUpdateTime(resultSet.getLastUpdateTime());
            }

            listView.dealLoadResult(this, errorMessage, dataList, true);
        }
    }

    public long getActionTime()
    {
        return this.actionTime;
    }
}