package craky.keeper.client;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import craky.keeper.util.KeeperUtil;
import craky.keeper.view.KeeperListView;
import craky.keeper.view.ListItem;

public class ContextMenuDialog extends DialogFragment implements View.OnClickListener
{
    private KeeperActivity context;

    private ListItem listItem;

    private Dialog dialog;

    private WindowManager.LayoutParams layoutParams;

    private Button btnModify, btnDelete;

    private KeeperListView listView;

    private DeleteConfirmDialog confirmDialog;

    private int deltaY;

    public ContextMenuDialog(KeeperActivity context, KeeperListView listView)
    {
        this.context = context;
        this.listView = listView;
    }

    @Override
    @SuppressLint("InflateParams")
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        if(dialog == null)
        {
            dialog = new Dialog(context, R.style.ContextMenuDialog);
            View contentView = context.getLayoutInflater().inflate(R.layout.context_menu, null);
            layoutParams = dialog.getWindow().getAttributes();
            layoutParams.gravity = Gravity.TOP;
            int width = contentView.getMinimumWidth();
            int height = contentView.getMinimumHeight();
            deltaY = height + KeeperUtil.getActivityTop(context);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(contentView);
            dialog.getWindow().setLayout(width, height);
            btnModify = (Button)dialog.findViewById(R.id.btnModify);
            btnDelete = (Button)dialog.findViewById(R.id.btnDelete);
            btnModify.setOnClickListener(this);
            btnDelete.setOnClickListener(this);
        }

        int[] location = new int[2];
        listItem.getLocationOnScreen(location);
        layoutParams.y = location[1] - deltaY;
        context.setDialog(dialog);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        PullDownListener pullDownListener = listView.getPullDownListener();

        if(pullDownListener != null)
        {
            pullDownListener.setPullable(true);
        }

        listItem.setUnselectable(true);
        listItem.setPressed(false);
        context.setDialog(null);
        super.onDismiss(dialog);
    }

    public ListItem getListItem()
    {
        return this.listItem;
    }

    public void setListItem(ListItem listItem)
    {
        listItem.setUnselectable(false);
        listItem.forcePressed();
        this.listItem = listItem;
    }

    @Override
    public void onClick(View view)
    {
        this.dismiss();

        if(view == btnModify)
        {
            listView.modifyItem(listItem);
        }
        else if(view == btnDelete)
        {
            if(confirmDialog == null)
            {
                confirmDialog = new DeleteConfirmDialog();
            }

            FragmentManager fm = context.getFragmentManager();
            confirmDialog.show(fm, null);
        }
    }

    private class DeleteConfirmDialog extends DialogFragment implements View.OnClickListener
    {
        private Dialog dialog;

        private Button btnOk;

        @SuppressLint("InflateParams")
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            if(dialog == null)
            {
                View view = context.getLayoutInflater().inflate(R.layout.confirm_dialog, null);
                dialog = new Dialog(context, R.style.ConfirmDialog);
                dialog.setContentView(view);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(false);
                WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
                layoutParams.height = view.getMinimumHeight();
                btnOk = (Button)dialog.findViewById(R.id.btnOk);
                btnOk.setOnClickListener(this);
                dialog.findViewById(R.id.btnCancel).setOnClickListener(this);
            }

            return dialog;
        }

        @Override
        public void onClick(View view)
        {
            this.dismiss();

            if(view == btnOk)
            {
                listView.deleteItem(listItem);
            }
        }
    }
}